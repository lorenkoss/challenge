namespace LeaseLock.Services.Enums
{
    public enum ErrorCodes
    {
        MORE_CERTS_THAN_UNITS,
        INVALID_PRODUCT_FOR_PROPERTY
    }
}
