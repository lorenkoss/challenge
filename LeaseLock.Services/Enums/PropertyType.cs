namespace LeaseLock.Services.Enums
{
    public enum PropertyType
    {
        PAY_IN_FULL,
        INSTALLMENTS
    }
}
