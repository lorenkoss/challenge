using System.Threading.Tasks;
using LeaseLock.Services.Models.ApiResponse;

namespace LeaseLock.Services.Interfaces
{
    public interface IApiClient
    {
        Task<CompanyResponse> GetCompany(int companyId);
        Task<CertResponse> GetCertificates(int companyId);
        Task<PropertyResponse> GetProperties(int companyId);
    }
}