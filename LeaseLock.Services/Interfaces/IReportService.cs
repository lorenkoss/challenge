using System;
using LeaseLock.Services.Models.ApiResponse;
using LeaseLock.Services.Models.Response;

namespace LeaseLock.Services.Interfaces
{
    public interface IReportService
    {
        Tuple<Report, Error> ProcessCompany(CompanyResponse company, CertResponse certificates, PropertyResponse properties);
    }
}