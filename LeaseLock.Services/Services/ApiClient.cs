using System;
using System.Net.Http;
using System.Threading.Tasks;
using LeaseLock.Services.Models.ApiResponse;
using LeaseLock.Services.Interfaces;
using Newtonsoft.Json;

namespace LeaseLock.Services.Services
{
    public class ApiClient : IApiClient
    {
        private readonly IHttpClientFactory _clientFactory;
        public ApiClient(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<CertResponse> GetCertificates(int companyId)
        {
            var httpClient = _clientFactory.CreateClient("leaselockchallenge");
            var apiReponse = await httpClient.GetAsync($"/prod/companies/{companyId}/certs");
            try
            {
                if (apiReponse.IsSuccessStatusCode) {
                    var responseBody = await apiReponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<CertResponse>(responseBody);
                }

            } catch(Exception ex) 
            {

            }
            return null;
        }

        public async Task<CompanyResponse> GetCompany(int companyId)
        {
            var httpClient = _clientFactory.CreateClient("leaselockchallenge");
            var apiReponse = await httpClient.GetAsync($"/prod/companies/{companyId}");
            try
            {
                if (apiReponse.IsSuccessStatusCode) {
                    var responseBody = await apiReponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<CompanyResponse>(responseBody);
                }

            } catch(Exception ex) 
            {

            }
            return null;

        }

        public async Task<PropertyResponse> GetProperties(int companyId)
        {
            var httpClient = _clientFactory.CreateClient("leaselockchallenge");
            var apiReponse = await httpClient.GetAsync($"/prod/companies/{companyId}/properties");
            try
            {
                if (apiReponse.IsSuccessStatusCode) {
                    var responseBody = await apiReponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<PropertyResponse>(responseBody);
                }

            } catch(Exception ex) 
            {

            }
            return null;
        }
    }
}