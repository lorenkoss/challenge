using System;
using System.Linq;
using LeaseLock.Services.Enums;
using LeaseLock.Services.Interfaces;
using LeaseLock.Services.Models.ApiResponse;
using LeaseLock.Services.Models.Response;

namespace LeaseLock.Services.Services
{
    public class ReportService : IReportService
    {
        public Tuple<Report, Error> ProcessCompany(CompanyResponse company, CertResponse certificates, PropertyResponse properties)
        {
            var report = new Report()
            {
                Company = company.Company.Name,
                TotalUnits = properties.Properties.Sum(x => x.Units),
                TotalCerts = certificates.Certs.Count,
            };
            Error error = null;

            report.TotalCoverage = report.TotalCerts / report.TotalUnits;
            var propIndex = 0;
            certificates.Certs.GroupBy(x => x.ProductId, x => x).ToList().ForEach(cert => {
                report.Certificates.Add(new LeaseLock.Services.Models.Response.Certificate() {
                    ProductId = cert.Key,
                    Amount =  cert.Count(),
                    Percent = cert.Count() / report.TotalCerts
                });
            });


            while (propIndex < properties.Properties.Count)
            {
                var prop = properties.Properties[propIndex++];
                var certs = certificates.Certs.Where(x => x.PropertyId == prop.Id).ToList();
                if (certs.Count > prop.Units) {
                    error = new Error() {
                        CompanyId = company.Company.Id,
                        ErrorCode = ErrorCodes.MORE_CERTS_THAN_UNITS.ToString()
                    };                    
                }
                if (prop.Type == PropertyType.PAY_IN_FULL)
                {
                    if (certs.Any(x => x.DownPayment == 0)) {
                        error = new Error() {
                            CompanyId = company.Company.Id,
                            ErrorCode = ErrorCodes.INVALID_PRODUCT_FOR_PROPERTY.ToString()
                        };
                    }
                } else {
                    if (certs.Any(x => x.InstallmentPayment == 0)) {
                        error = new Error() {
                            CompanyId = company.Company.Id,
                            ErrorCode = ErrorCodes.INVALID_PRODUCT_FOR_PROPERTY.ToString()
                        };

                    }
                }
                
                if (error != null) {
                    propIndex = properties.Properties.Count;
                    report = null;
                    continue;
                };

                var monthRevenue = prop.Type == PropertyType.PAY_IN_FULL ? 
                        certs.Sum(x => x.DownPayment)/12 : certs.Sum(x => x.InstallmentPayment + x.MonthlyFee);
                report.Properties.Add(new LeaseLock.Services.Models.Response.Property() {
                    Certs = certs.Count,
                    PropertyId = prop.Id,
                    Units = prop.Units,
                    Coverage = certs.Count / prop.Units,
                    MonthlyRevenue = monthRevenue
                });
                report.MonthRevenue += monthRevenue;
                report.AnnualRevenue += monthRevenue * 12;
            }
            return new Tuple<Report, Error>(report, error);
        }
        
    }
}