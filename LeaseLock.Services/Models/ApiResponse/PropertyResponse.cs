using System.Collections.Generic;

namespace LeaseLock.Services.Models.ApiResponse
{
    public class PropertyResponse : BaseApiResponse
    {
        public List<Property> Properties {get; set;} 
    }
}