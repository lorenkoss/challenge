using System.Collections.Generic;

namespace LeaseLock.Services.Models.ApiResponse
{
    public class CertResponse : BaseApiResponse
    {
        public List<Certificate> Certs {get; set;}
    }
}