namespace LeaseLock.Services.Models.ApiResponse
{
    public class Company
    {
        public string Id {get; set;}
        public string Name {get; set;}
    }
}