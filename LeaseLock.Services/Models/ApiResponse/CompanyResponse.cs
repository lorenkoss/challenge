namespace LeaseLock.Services.Models.ApiResponse
{
    public class CompanyResponse : BaseApiResponse
    {
        public Company Company {get; set;}
    }
}