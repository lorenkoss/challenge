using System.Collections.Generic;
using LeaseLock.Services.Enums;

namespace LeaseLock.Services.Models.ApiResponse
{

    public class Property
    {
        public string Id {get; set;}
        public PropertyType Type {get; set;}
        public double Units {get; set;} 
    }
}