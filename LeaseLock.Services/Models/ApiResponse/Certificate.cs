using Newtonsoft.Json;

namespace LeaseLock.Services.Models.ApiResponse
{
    public class Certificate
    {
        [JsonProperty("down_payment")]
        public double DownPayment {get; set;}
        public string Id {get; set;}
        [JsonProperty("installment_payment")]
        public double InstallmentPayment {get; set;}
        [JsonProperty("monthly_fee")]
        public double MonthlyFee {get; set;}
        [JsonProperty("property_id")]
        public string PropertyId {get; set;}
        [JsonProperty("product_id")]
        public string ProductId {get; set;}

    }
}