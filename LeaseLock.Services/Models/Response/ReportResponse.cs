using System.Collections.Generic;
using Newtonsoft.Json;

namespace LeaseLock.Services.Models.Response
{
    public class ReportResponse
    {
        [JsonProperty("report")]
        public List<Report> Report {get; } = new List<Report>();
        [JsonProperty("errors")]
        public List<Error> Errors {get; } = new List<Error>();
    }
}