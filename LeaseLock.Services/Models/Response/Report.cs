using System.Collections.Generic;
using Newtonsoft.Json;

namespace LeaseLock.Services.Models.Response
{
    public class Report
    {
        [JsonProperty("company_name")]
        public string Company {get; set;}
        [JsonProperty("total_units")]
        public double TotalUnits {get; set;}
        [JsonProperty("total_certs")]
        public double TotalCerts {get; set;}
        [JsonProperty("total_coverage")]
        public double TotalCoverage {get; set;}
        [JsonProperty("monthly_revenue")]
        public double MonthRevenue {get; set;}
        [JsonProperty("annual_revenue")]
        public double AnnualRevenue {get; set;}
        [JsonProperty("properties")]
        public List<Property> Properties {get; } = new List<Property>();
        [JsonProperty("certs")]
        public List<Certificate> Certificates {get; } = new List<Certificate>();
    }
}