namespace LeaseLock.Services.Models.Response
{
    public class Company
    {
        public string Id {get; set;}
        public string Name {get; set;}
    }
}