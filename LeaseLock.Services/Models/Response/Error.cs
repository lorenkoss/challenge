using Newtonsoft.Json;

namespace LeaseLock.Services.Models.Response
{
    public class Error
    {
        [JsonProperty("error_code")]
        public string ErrorCode {get; set;}
        [JsonProperty("company_id")]
        public string CompanyId {get;set;}
    }
}