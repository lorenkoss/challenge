using Newtonsoft.Json;

namespace LeaseLock.Services.Models.Response
{
    public class Certificate
    {
        [JsonProperty("product_id")]
        public string ProductId {get; set;}
        public double Amount {get; set;}
        public double Percent {get; set;}
    }
}