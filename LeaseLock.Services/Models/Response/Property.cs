using Newtonsoft.Json;

namespace LeaseLock.Services.Models.Response
{
    public class Property
    {
        [JsonProperty("property_id")]
        public string PropertyId {get; set;}
        [JsonProperty("certs")]
        public double Certs {get; set;}
        [JsonProperty("units")]
        public double Units {get; set;}
        [JsonProperty("coverage")]
        public double Coverage {get; set;}
        [JsonProperty("monthly_revenue")]
        public double MonthlyRevenue {get; set;}
    }
}