using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Linq;
using System;
using LeaseLock.Services.Interfaces;
using System.Collections.Generic;
using LeaseLock.Services.Models.ApiResponse;
using LeaseLock.Services.Models.Response;
using Newtonsoft.Json;
using System.IO;

namespace LeaseLockChallenge
{
    public class ReportCompilerService : IHostedService
    {
        private readonly IConfiguration _configuration;
        private readonly IHostApplicationLifetime _appLifeTime;
        private readonly IApiClient _apiClient;
        private readonly IReportService _reportService;
        private readonly ICommandLineArgs _commandLineArgs;
        public ReportCompilerService(
                ICommandLineArgs commandLineArgs,
                IApiClient apiClient,
                IReportService reportService,
                IHostApplicationLifetime appLifeTime)
        {
            _reportService = reportService;
            _apiClient = apiClient;
            _appLifeTime = appLifeTime;
            _commandLineArgs = commandLineArgs;
        }
        public  string JsonPrettify(string json)
        {
            using (var stringReader = new StringReader(json))
            using (var stringWriter = new StringWriter())
            {
                var jsonReader = new JsonTextReader(stringReader);
                var jsonWriter = new JsonTextWriter(stringWriter) { Formatting = Formatting.Indented };
                jsonWriter.WriteToken(jsonReader);
                return stringWriter.ToString();
            }
        }
        private Task DoWork(ReportResponse reportResponse, CancellationToken cancellationToken)
        {
            // var ids = _configuration.AsEnumerable().Where(x => x.Key == "ids").Select(x => x.Value).FirstOrDefault();
            // if (string.IsNullOrEmpty(ids)) {
            //     Console.WriteLine("No ids entered.  use --ids <id1,id2,id3..>");
            //     _appLifeTime.StopApplication();
            //     return Task.CompletedTask;
            // }
            var tasks = new List<Task>();

            _commandLineArgs.Ids.Split(",").ToList().ForEach(id => {
                tasks.Clear();
                if (int.TryParse(id, out var idInt)) {
                    
                    tasks.Add(_apiClient.GetCompany(idInt));
                    tasks.Add(_apiClient.GetCertificates(idInt));
                    tasks.Add(_apiClient.GetProperties(idInt));

                    Task.WaitAll(tasks.ToArray());

                    var company = ((Task<CompanyResponse>)tasks[0]).Result;
                    var certificates = ((Task<CertResponse>)tasks[1]).Result;
                    var properties = ((Task<PropertyResponse>)tasks[2]).Result;

                    var output = new ReportResponse();
                    var result = _reportService.ProcessCompany(company, certificates, properties);
                    if (result.Item1 != null) {
                        reportResponse.Report.Add(result.Item1);
                    }
                    if (result.Item2 != null) {
                        reportResponse.Errors.Add(result.Item2);
                    }
                }
            });
            return Task.CompletedTask;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _appLifeTime.ApplicationStarted.Register(() => {
                var reportResponse = new ReportResponse();
                DoWork(reportResponse, cancellationToken);
                Console.WriteLine(JsonPrettify(JsonConvert.SerializeObject(reportResponse)));
                _appLifeTime.StopApplication();
            });
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}