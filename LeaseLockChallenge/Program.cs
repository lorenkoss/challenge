﻿using System;
using System.Threading.Tasks;
using LeaseLock.Services.Interfaces;
using LeaseLock.Services.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LeaseLockChallenge
{
    class Program
    {
        private static IConfiguration Configuration { get; set; }
        static async Task Main(string[] args)
        {
            if (args.Length != 1) {
                Console.WriteLine("No ids entered.  use --ids <id1,id2,id3..>");
                return;
            }
            await CreateHost(args).RunConsoleAsync();
        }
        private static IHostBuilder CreateHost(string[] args)
        {

            return new HostBuilder()
                .ConfigureServices((HostBuilderContext, services) =>
                {
                    services.AddSingleton<IConfiguration>(provider => Configuration);
                    services.AddSingleton<ICommandLineArgs>(provider => {
                        return new CommandLineArgs() {
                            Ids = args[0]
                        };
                    });
                    services.AddHostedService<ReportCompilerService>();
                    services.AddSingleton<IApiClient, ApiClient>();
                    services.AddSingleton<IReportService, ReportService>();
                    services.AddHttpClient("leaselockchallenge", configure => {
                        configure.BaseAddress = new Uri("https://idy4d4ejzi.execute-api.us-west-2.amazonaws.com/");
                    });


                });
        }
    }
}
