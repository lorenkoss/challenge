﻿using System;
using LeaseLock.Services.Interfaces;
using LeaseLock.Services.Services;
using Microsoft.Extensions.DependencyInjection;;

namespace LeaseLock.Tests
{
    class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IReportService, ReportService>();

        }
    }
}
