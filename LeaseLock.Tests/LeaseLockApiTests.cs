using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System;
using LeaseLock.Services.Interfaces;
using Newtonsoft.Json;
using LeaseLock.Services.Models.ApiResponse;
using System.IO;
using LeaseLock.Services.Enums;

namespace LeaseLock.Tests
{
    public class LeaseLockApiTests
    {
        private readonly IServiceProvider _provider;
        public LeaseLockApiTests()
        {
            var services = new ServiceCollection();
            var startup = new Startup();
            startup.ConfigureServices(services);
            _provider = services.BuildServiceProvider();
        }

        [Fact]
        public void TestMoreCertsThanUnits()
        {
            var reportService = _provider.GetRequiredService<IReportService>();


            var company = JsonConvert.DeserializeObject<CompanyResponse>(File.ReadAllText("Tests\\company.json"));
            var certificates = JsonConvert.DeserializeObject<CertResponse>(File.ReadAllText("Tests\\morecertsthanunits.json"));
            var properties = JsonConvert.DeserializeObject<PropertyResponse>(File.ReadAllText("Tests\\morecertsthanunitsproperties.json"));

            var result = reportService.ProcessCompany(company, certificates, properties);
            Assert.NotNull(result.Item2);
            Assert.Equal(result.Item2.ErrorCode, ErrorCodes.MORE_CERTS_THAN_UNITS.ToString());
        }
        [Fact]
        public void TestCertsLessThanUnits()
        {
            var reportService = _provider.GetRequiredService<IReportService>();


            var company = JsonConvert.DeserializeObject<CompanyResponse>(File.ReadAllText("Tests\\company.json"));
            var certificates = JsonConvert.DeserializeObject<CertResponse>(File.ReadAllText("Tests\\certs.json"));
            var properties = JsonConvert.DeserializeObject<PropertyResponse>(File.ReadAllText("Tests\\properties.json"));

            var result = reportService.ProcessCompany(company, certificates, properties);
            Assert.NotNull(result.Item1);
        }
        [Fact]
        public void TestInvalidInstallmentProperty()
        {
            var reportService = _provider.GetRequiredService<IReportService>();


            var company = JsonConvert.DeserializeObject<CompanyResponse>(File.ReadAllText("Tests\\company.json"));
            var certificates = JsonConvert.DeserializeObject<CertResponse>(File.ReadAllText("Tests\\invalidpropcert.json"));
            var properties = JsonConvert.DeserializeObject<PropertyResponse>(File.ReadAllText("Tests\\invalidpropertyproperties.json"));

            var result = reportService.ProcessCompany(company, certificates, properties);
            Assert.NotNull(result.Item2);
            Assert.Equal(result.Item2.ErrorCode, ErrorCodes.INVALID_PRODUCT_FOR_PROPERTY.ToString());
        }
        [Fact]
        public void TestInvalidPaidInFullProperty()
        {
            var reportService = _provider.GetRequiredService<IReportService>();


            var company = JsonConvert.DeserializeObject<CompanyResponse>(File.ReadAllText("Tests\\company.json"));
            var certificates = JsonConvert.DeserializeObject<CertResponse>(File.ReadAllText("Tests\\invalidpropcert2.json"));
            var properties = JsonConvert.DeserializeObject<PropertyResponse>(File.ReadAllText("Tests\\invalidpropertyproperties.json"));

            var result = reportService.ProcessCompany(company, certificates, properties);
            Assert.NotNull(result.Item2);
            Assert.Equal(result.Item2.ErrorCode, ErrorCodes.INVALID_PRODUCT_FOR_PROPERTY.ToString());
        }
    }
}