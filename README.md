# LeaseLock Challenge
By Loren Koss
May 3, 2021

# To Build
```
clone repository
cd LeaseLockChallenge
dotnet build
```

# To Run (LeaseLockChallenge app)
```
dotnet run #,#,# (comma seperated list of ids)
```

# To Test
```
cd LeaseLock.Tests
dotnet test
```
### tests
1. TestMoreCertsThanUnits (validates an error is returned when there are more certificates than units for a property)
2. TestCertsLessThanUnits (validates no error if certs are less than units)
3. TestInvalidInstallmentProperty (validates an error is returned if installment type and installment amount = 0)
4. TestInvalidPaidInFullProperty (validates an error is returned if paid in full and down_payment = 0)

